# Zhconvert

#### 介绍
中文简繁体转换

#### 安装教程

1.  使用 [Composer](https://getcomposer.org/)

`composer require yjqqkia/zhconvert`

2. 复制GIT仓库/下载zip 到本地

`git clone https://gitee.com/liumingjang/zhconvert.git`

在`zhconvert`目录下生成自动加载文件 `autoload.php`

`composer dump-autoload`

#### 使用说明
```
<?php
require_once ('./vendor/autoload.php');

use yjqqkia\zhconvert\ZhConvert;

echo ZhConvert::convert('皇后','zh-hk'),"\r\n";
echo ZhConvert::convert('后来哦','zh-hk'),"\r\n";
```
目前提供的转换类型有

`zh-hant`:转繁体

`zh-hans`:转简体

`zh-hk`:转港澳繁体

`zh-tw`:转台湾繁体

`zh-sg`:转大陆简体
