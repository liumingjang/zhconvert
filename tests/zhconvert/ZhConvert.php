<?php

namespace yjqqkia\zhconvert;


use yjqqkia\zhconvert\Dict;

class ZhConvert
{
    /**
     * Convert in action.
     *
     * @param string $str       text to be converted
     * @param string $variant   target language code, e.g. zh, zh-cn, zh-tw, zh-sg & zh-hk
     *
     * @return string the converted text
     */
    public static function convert($string, $languageCode)
    {
        switch ($languageCode) {
            case 'zh-hant': //转繁体
                $string = strtr($string, Dict::$zh2Hant);
                break;
            case 'zh-hans': //转简体
                $string = strtr($string, Dict::$zh2Hans);
                break;
            case 'zh-hk':   //转港澳繁体
                $string = strtr($string, Dict::$zh2HK);
                $string = strtr($string, Dict::$zh2Hant);
                break;
            case 'zh-tw':   //转台湾繁体
                $string = strtr($string, Dict::$zh2TW);
                $string = strtr($string, Dict::$zh2Hant);
                break;
            case 'zh-sg':   //转大陆简体
                $string = strtr($string, Dict::$zh2CN);
                $string = strtr($string, Dict::$zh2Hans);
                break;
        }
        return $string;
    }
}
